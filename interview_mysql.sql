-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2022 at 05:10 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `interview`
--
CREATE DATABASE `interview` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `interview`;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `ISBN` varchar(30) NOT NULL,
  `BOOKNAME` longtext NOT NULL,
  `AUTHOR` longtext NOT NULL,
  `BOOKQTY` int(4) NOT NULL,
  `ISDELETED` varchar(2) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ISBN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`ISBN`, `BOOKNAME`, `AUTHOR`, `BOOKQTY`, `ISDELETED`) VALUES
('111-1-111-111-1', 'My Diary', 'Harfirie', 1, 'Y'),
('978-0-141-35507-8', 'The Fault In Our Stars', 'John Green', 1, 'N'),
('978-0-385-49649-0', 'Tuesday With Morrie', 'Mitch Albom', 4, 'N'),
('978-0-552-16500-6', 'What''''s Your Number?', 'Karyn Bosnak', 1, 'N'),
('978-1-4711-1614-8', 'The Perks of Being a Wallflower', 'Stephen Chbosky', 1, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `books_qty`
--

CREATE TABLE IF NOT EXISTS `books_qty` (
  `QTYID` int(4) NOT NULL AUTO_INCREMENT,
  `STATUS` varchar(10) NOT NULL,
  `ISBN` varchar(30) NOT NULL,
  PRIMARY KEY (`QTYID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `books_qty`
--

INSERT INTO `books_qty` (`QTYID`, `STATUS`, `ISBN`) VALUES
(1, 'AVAILABLE', '978-0-385-49649-0'),
(2, 'AVAILABLE', '978-0-385-49649-0'),
(3, 'AVAILABLE', '978-0-385-49649-0'),
(4, 'AVAILABLE', '978-0-385-49649-0'),
(6, 'AVAILABLE', '978-0-141-35507-8'),
(7, 'AVAILABLE', '978-0-552-16500-6'),
(8, 'AVAILABLE', '978-1-4711-1614-8');

-- --------------------------------------------------------

--
-- Table structure for table `borrowed`
--

CREATE TABLE IF NOT EXISTS `borrowed` (
  `BID` int(4) NOT NULL AUTO_INCREMENT,
  `ISBN` varchar(30) NOT NULL,
  `DUEDATE` longtext NOT NULL,
  `DATERETURN` longtext NOT NULL,
  `MID` int(4) NOT NULL,
  PRIMARY KEY (`BID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `borrowed`
--

INSERT INTO `borrowed` (`BID`, `ISBN`, `DUEDATE`, `DATERETURN`, `MID`) VALUES
(1, '978-0-385-49649-0', '14-01-2022', '07-01-2022', 1),
(2, '978-0-141-35507-8', '15-01-2022', '08-01-2022', 1),
(3, '978-0-141-35507-8', '15-01-2022', '08-01-2022', 1),
(4, '978-0-141-35507-8', '15-01-2022', '09-01-2022', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `MID` int(4) NOT NULL AUTO_INCREMENT,
  `NAME` longtext NOT NULL,
  `TEL` varchar(15) NOT NULL,
  `EMAIL` longtext NOT NULL,
  `ACCOUNTSTATUS` varchar(8) NOT NULL,
  PRIMARY KEY (`MID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`MID`, `NAME`, `TEL`, `EMAIL`, `ACCOUNTSTATUS`) VALUES
(1, 'Ahmad Danise', ' 60166412374', 'ahmad.dani@gmail.com', 'ACTIVE');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
