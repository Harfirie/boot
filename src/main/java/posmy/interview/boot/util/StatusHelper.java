package posmy.interview.boot.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import posmy.interview.boot.response.BaseStatus;


public class StatusHelper {
	public static BaseStatus SUCCESS() {
		BaseStatus status = new BaseStatus();
		status.setCode(200);
		status.setType("OK");
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return status;
	}
	
	public static BaseStatus SUCCESS_CUSTOM(String message) {
		BaseStatus status = new BaseStatus();
		status.setCode(200);
		status.setType("OK");
		status.setMessage(message);
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return status;
	}

	public static BaseStatus NOT_FOUND(String message) {
		BaseStatus status = new BaseStatus();
		status.setCode(404);
		status.setType("NOT_FOUND");
		status.setError(message);
		status.setTimestamp(GeneratorUtil.getTimestamp());
		return status;
	}
	
	public static BaseStatus CONFLICT(String message) {
		BaseStatus status = new BaseStatus();
		status.setCode(409);
		status.setType("CONFLICT");
		status.setError(message);
		status.setTimestamp(GeneratorUtil.getTimestamp());
		return status;
	}

	public static ResponseEntity<?> GATEWAY_TIMEOUT() {
		BaseStatus status = new BaseStatus();
		status.setCode(504);
    	status.setType("NOk");
    	status.setError("Tibco is Down!");
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return ResponseEntity.status(HttpStatus.GATEWAY_TIMEOUT).body(status);
	}

	public static ResponseEntity<?> INTERNAL_SERVER_ERROR() {
		BaseStatus status = new BaseStatus();
		status.setCode(500);
    	status.setType("NOk");
    	status.setError("Internal Server Error!");
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(status);
	}
	
	public static ResponseEntity<?> CUSTOM_INTERNAL_SERVER_ERROR(String message) {
		BaseStatus status = new BaseStatus();
		status.setCode(500);
    	status.setType("NOk");
    	status.setError(message);
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(status);
	}

	public static ResponseEntity<?> BAD_REQUEST() {
		BaseStatus status = new BaseStatus();
		status.setCode(400);
    	status.setType("BAD_REQUEST");
    	status.setError("Incomplete Request Parameter!");
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(status);
	}
	
	public static ResponseEntity<?> BAD_REQUEST(String message) {
		BaseStatus status = new BaseStatus();
		status.setCode(400);
    	status.setType("BAD_REQUEST");
    	status.setError(message);
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(status);
	}

	public static ResponseEntity<?> UNAUTHORIZED() {
		BaseStatus status = new BaseStatus();
		status.setCode(401);
    	status.setType("NOk");
    	status.setError("Unauthorized!");
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(status);
	}
	
	public static ResponseEntity<?> NO_CONTENT() {
		BaseStatus status = new BaseStatus();
		status.setCode(204);
    	status.setType("NOk");
    	status.setError("No content or data");
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(status);
	}
	
	public static ResponseEntity<?> INVALID_FORMAT(String message) {
		BaseStatus status = new BaseStatus();
		status.setCode(422);
    	status.setType("NOk");
    	status.setError(message);
		status.setTimestamp(GeneratorUtil.getTimestamp());

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(status);
	}
}
