package posmy.interview.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import posmy.interview.boot.common.Constants;
import posmy.interview.boot.model.BooksModel;
import posmy.interview.boot.service.LibrarianService;
import posmy.interview.boot.util.StatusHelper;

/**
 * @author amirul.harfirie
 */

@Slf4j
@RestController
@RequestMapping(Constants.BASE_URL)
public class LibrarianController {

	@Autowired
	LibrarianService service;
	
	@RequestMapping(path=Constants.VIEW_BOOKS, method = RequestMethod.GET)
    public ResponseEntity<?> ViewBooks() throws Exception{
		log.info(":: LibrarianController - AddBooks - Started ::");
		ResponseEntity<?> response = null;
		try {
			response = service.getAllBooksService();
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
	
	@RequestMapping(path=Constants.ADD_BOOK, method = RequestMethod.POST)
    public ResponseEntity<?> AddBooks(@RequestParam(required = true) String ISBN, String bookName, String author, String booksQty) throws Exception{
		log.info(":: LibrarianController - AddBooks - Started ::");
		ResponseEntity<?> response = null;
		BooksModel book = new BooksModel();
		try {
			book.setISBN(ISBN);
			book.setBookName(bookName);
			book.setAuthor(author);
			book.setBooksQty(booksQty);
			response = service.getAddBooksService(book, "add");
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
	
	@RequestMapping(path=Constants.UPDATE_BOOK, method = RequestMethod.PUT)
    public ResponseEntity<?> UpdateBooks(@RequestParam(required = true) String ISBN, String bookName, String author) throws Exception{
		log.info(":: LibrarianController - UpdateBooks - Started ::");
		ResponseEntity<?> response = null;
		BooksModel book = new BooksModel();
		try {
			book.setISBN(ISBN);
			book.setBookName(bookName);
			book.setAuthor(author);
			response = service.getAddBooksService(book, "update");
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
	
	@RequestMapping(path=Constants.REMOVE_BOOK, method = RequestMethod.DELETE)
    public ResponseEntity<?> RemoveBooks(@RequestParam(required = true) String ISBN, @RequestParam(required = true)String quantity) throws Exception{
		log.info(":: LibrarianController - RemoveBooks - Started ::");
		ResponseEntity<?> response = null;
		BooksModel book = new BooksModel();
		try {
			book.setISBN(ISBN);
			book.setBooksQty(quantity);
			response = service.getAddBooksService(book, "remove");
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
	
	@RequestMapping(path=Constants.BORROW, method = RequestMethod.POST)
    public ResponseEntity<?> BorrowBooks(@RequestParam(required = true) String Tel, String ISBN) throws Exception{
		log.info(":: LibrarianController - BorrowBooks - Started ::");
		ResponseEntity<?> response = null;
		try {
			response = service.getBorrowService(Tel, ISBN);
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
	
	@RequestMapping(path=Constants.RETURN, method = RequestMethod.PUT)
    public ResponseEntity<?> ReturnBooks(@RequestParam(required = true) String Tel, String ISBN) throws Exception{
		log.info(":: LibrarianController - ReturnBooks - Started ::");
		ResponseEntity<?> response = null;
		try {
			response = service.getReturnService(Tel, ISBN);
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
}
