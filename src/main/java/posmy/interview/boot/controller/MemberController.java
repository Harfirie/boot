package posmy.interview.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import posmy.interview.boot.common.Constants;
import posmy.interview.boot.model.MemberModel;
import posmy.interview.boot.service.MemberService;
import posmy.interview.boot.util.StatusHelper;

/**
 * @author amirul.harfirie
 */

@Slf4j
@RestController
@RequestMapping(Constants.BASE_URL)
public class MemberController {

	@Autowired
	MemberService service;
	
	@RequestMapping(path=Constants.VIEW_MEMBERS, method = RequestMethod.GET)
    public ResponseEntity<?> ViewMember() throws Exception{
		log.info(":: MemberController - ViewMember - Started ::");
		ResponseEntity<?> response = null;
		try {
			response = service.getAllMemberService();
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
	
	@RequestMapping(path=Constants.ADD_MEMBER, method = RequestMethod.POST)
    public ResponseEntity<?> AddMember(@RequestParam(required = true) String Name, @RequestParam(required = true)String Tel, @RequestParam(required = true)String Email) throws Exception{
		log.info(":: MemberController - AddMember - Started ::");
		ResponseEntity<?> response = null;
		try {
			MemberModel m = new MemberModel();
			m.setName(Name);
			m.setTel(Tel);
			m.setEmail(Email);
			response = service.getAddMemberService(m, "add");
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
	
	@RequestMapping(path=Constants.UPDATE_MEMBER, method = RequestMethod.PUT)
    public ResponseEntity<?> UpdateMember(@RequestParam(required = true) String Name, @RequestParam(required = true)String Tel, @RequestParam(required = true)String Email) throws Exception{
		log.info(":: MemberController - UpdateMember - Started ::");
		ResponseEntity<?> response = null;
		try {
			MemberModel m = new MemberModel();
			m.setName(Name);
			m.setTel(Tel);
			m.setEmail(Email);
			response = service.getAddMemberService(m, "update");
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
	
	@RequestMapping(path=Constants.REMOVE_MEMBER, method = RequestMethod.DELETE)
    public ResponseEntity<?> RemoveMember(@RequestParam(required = true)String Tel) throws Exception{
		log.info(":: MemberController - RemoveMember - Started ::");
		ResponseEntity<?> response = null;
		try {
			MemberModel m = new MemberModel();
			m.setTel(Tel);
			response = service.getAddMemberService(m, "remove");
		}
		catch(Exception e) {
			e.printStackTrace();
			response = StatusHelper.INTERNAL_SERVER_ERROR();
		}
        return response;
    }
}
