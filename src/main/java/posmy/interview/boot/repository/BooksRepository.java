package posmy.interview.boot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import posmy.interview.boot.entity.BooksEntity;

@Repository
public interface BooksRepository extends JpaRepository<BooksEntity, Long>{

	@Query("select a from BooksEntity a where isbn=?1 and isdeleted=?2")
	public List<BooksEntity> searchBook(String isbn, String isdeleted);

	public void deleteByIsbn(String isbn);

	@Query(value = "DELETE FROM books WHERE isbn=?1", nativeQuery = true)
	public void deleteIsbn(String isbn);

	public List<BooksEntity> findAllByIsDeleted(String isdeleted);

}
