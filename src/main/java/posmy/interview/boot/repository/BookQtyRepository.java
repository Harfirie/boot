package posmy.interview.boot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import posmy.interview.boot.entity.BooksQtyEntity;

@Repository
public interface BookQtyRepository  extends JpaRepository<BooksQtyEntity, Long>{

	List<BooksQtyEntity> findAllByIsbnAndStatus(String isbn, String status);

	void deleteByIsbn(String isbn);

	List<BooksQtyEntity> findAllByIsbn(String isbn);

}
