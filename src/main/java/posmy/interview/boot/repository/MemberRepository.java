package posmy.interview.boot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import posmy.interview.boot.entity.MemberEntity;

@Repository
public interface MemberRepository extends JpaRepository<MemberEntity, Long>{

	List<MemberEntity> findByTel(String tel);

	List<MemberEntity> findByAccountStatus(String accountStatus);

	@Query(value="select * from member where TEL=?1 and ACCOUNTSTATUS=?2", nativeQuery = true)
	List<MemberEntity> searchTelAndAccountstatus(String tel, String accstatus);

}
