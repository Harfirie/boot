package posmy.interview.boot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import posmy.interview.boot.entity.BorrowEntity;

@Repository
public interface BorrowRepository extends JpaRepository<BorrowEntity, Long>{

	List<BorrowEntity> findByMidAndDateReturnAndIsbn(Long mid, String datereturn, String isbn);

}
