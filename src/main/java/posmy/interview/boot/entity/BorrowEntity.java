package posmy.interview.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import posmy.interview.boot.common.Constants;

@Getter
@Setter
@Entity
@Table(name = Constants.TABLE_BORROWED)
public class BorrowEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BID")
	private Long bid;
	
	@Column(name = "ISBN")
	private String isbn;
	
	@Column(name = "DUEDATE")
	private String dueDate;
	
	@Column(name = "DATERETURN")
	private String dateReturn;
	
	@Column(name = "MID")
	private Long mid;
}
