package posmy.interview.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import posmy.interview.boot.common.Constants;

@Getter
@Setter
@Entity
@Table(name = Constants.TABLE_BOOKS)
public class BooksEntity {

	@Id
	@Column(name = "ISBN")
	private String isbn;
	
	@Column(name = "BOOKNAME")
	private String bookName;
	
	@Column(name = "AUTHOR")
	private String author;
	
	@Column(name = "BOOKQTY")
	private String booksQty;
	
	@Column(name = "ISDELETED")
	private String isDeleted;
}
