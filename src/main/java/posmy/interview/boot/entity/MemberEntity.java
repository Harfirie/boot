package posmy.interview.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import posmy.interview.boot.common.Constants;

@Getter
@Setter
@Entity
@Table(name = Constants.TABLE_MEMBER)
public class MemberEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MID")
	private Long mid;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "TEL")
	private String tel;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "ACCOUNTSTATUS")
	private String accountStatus;
}
