package posmy.interview.boot;

import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;		

@SpringBootApplication
//@EnableEncryptableProperties
@EnableCaching
@EnableScheduling
public class BootApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(BootApplication.class);
        app.setDefaultProperties(getProperties());
        app.run(args);
	}

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BootApplication.class).properties(getProperties());
    }
    
    static Properties getProperties() {
    	Properties props = new Properties();
    	props.put("spring.config.name", "application");
    	return props;
    }
}
