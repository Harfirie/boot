package posmy.interview.boot.common;

public class Constants {

	// end-point
	public static final String BASE_URL = "/api/interview";
	public static final String ADD_BOOK = "/book/add";
	public static final String UPDATE_BOOK = "/book/update";
	public static final String REMOVE_BOOK = "/book/remove";
	public static final String VIEW_BOOKS = "/book/view";
	public static final String VIEW_MEMBERS = "/member/view";
	public static final String ADD_MEMBER = "/member/add";
	public static final String UPDATE_MEMBER = "/member/update";
	public static final String REMOVE_MEMBER = "/member/remove";
	public static final String REMOVE_ACCOUNT = "/member/delete";
	public static final String BORROW = "/book/borrow";
	public static final String RETURN = "/book/return";
	
	// list of table
	public static final String TABLE_BOOKS = "books";
	public static final String TABLE_BOOKS_QTY = "books_qty";
	public static final String TABLE_MEMBER = "member";
	public static final String TABLE_BORROWED = "borrowed";
}
