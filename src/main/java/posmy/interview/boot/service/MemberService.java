package posmy.interview.boot.service;

import org.springframework.http.ResponseEntity;

import posmy.interview.boot.model.MemberModel;

public interface MemberService {

	public ResponseEntity<?> getAddMemberService(MemberModel m, String flag) throws Exception;

	public ResponseEntity<?> getAllMemberService() throws Exception;

}
