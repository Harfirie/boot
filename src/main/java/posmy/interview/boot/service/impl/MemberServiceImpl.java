package posmy.interview.boot.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import posmy.interview.boot.entity.MemberEntity;
import posmy.interview.boot.model.MemberModel;
import posmy.interview.boot.repository.MemberRepository;
import posmy.interview.boot.response.ViewMemberResponse;
import posmy.interview.boot.service.MemberService;
import posmy.interview.boot.util.StatusHelper;

@Slf4j
@Service
public class MemberServiceImpl implements MemberService{

	@Autowired
	MemberRepository repo;

	@Override
	public ResponseEntity<?> getAllMemberService() throws Exception {
		log.info(":: getAllMemberService - Started ::");
		ResponseEntity<?> response = null;
		List<MemberEntity> result = new ArrayList<MemberEntity>();
		List<MemberModel> ml = new ArrayList<MemberModel>();
		ViewMemberResponse vmr = new ViewMemberResponse();
		try {
			result = repo.findByAccountStatus("ACTIVE");
			if(result.size() > 0) {
				for(MemberEntity t : result) {
					MemberModel m = new MemberModel();
					m.setName(t.getName());
					m.setTel(t.getTel());
					m.setEmail(t.getEmail());
					m.setAccountStatus(t.getAccountStatus());
					ml.add(m);
				}
				vmr.Status = StatusHelper.SUCCESS();
				vmr.Response = ml;
				response = ResponseEntity.status(HttpStatus.OK).body(vmr);
			}
			else {
				response = ResponseEntity.status(HttpStatus.NOT_FOUND).body(StatusHelper.NOT_FOUND("Record not found!"));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseEntity<?> getAddMemberService(MemberModel m, String flag) throws Exception {
		log.info(":: getAddMemberService - Started ::");
		ResponseEntity<?> response = null;
		List<MemberEntity> result = new ArrayList<MemberEntity>();
		try {
			result = repo.findByTel(m.getTel());
			if (result.size() > 0) {
				// update member account
				if("update".equals(flag)) {
					MemberEntity ment = new MemberEntity();
					ment.setMid(result.get(0).getMid());
					ment.setName(m.getName());
					ment.setTel(result.get(0).getTel());
					ment.setEmail(m.getEmail());
					ment.setAccountStatus("ACTIVE");
					repo.save(ment);
				}
				else if("remove".equals(flag)){
					MemberEntity ment = new MemberEntity();
					ment.setMid(result.get(0).getMid());
					ment.setName(result.get(0).getName());
					ment.setTel(result.get(0).getTel());
					ment.setEmail(result.get(0).getEmail());
					ment.setAccountStatus("INACTIVE");
					repo.save(ment);
				}
				
				if("add".equals(flag)) {
					response = ResponseEntity.status(HttpStatus.CONFLICT).body(StatusHelper.CONFLICT(m.getTel() + " is already existed!"));
				}
				else if("update".equals(flag)) {
					response = ResponseEntity.status(HttpStatus.OK).body(StatusHelper.SUCCESS_CUSTOM(m.getName() + " account is successfully updated!"));
				}
				else {
					response = ResponseEntity.status(HttpStatus.OK).body(StatusHelper.SUCCESS_CUSTOM(result.get(0).getName() + " account is successfully removed!"));
				}
			} 
			else {
				// create member account
				if("add".equals(flag)) {
					MemberEntity ment = new MemberEntity();
					ment.setName(m.getName());
					ment.setTel(m.getTel());
					ment.setEmail(m.getEmail());
					ment.setAccountStatus("ACTIVE");
					repo.save(ment);
					response = ResponseEntity.status(HttpStatus.OK).body(StatusHelper.SUCCESS_CUSTOM(m.getName() + " is successfully registered!"));
				}
				else{
					response = ResponseEntity.status(HttpStatus.NOT_FOUND).body(StatusHelper.NOT_FOUND("+"+m.getTel() + " account cannot be found!"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

}
