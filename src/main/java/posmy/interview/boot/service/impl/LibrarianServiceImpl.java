package posmy.interview.boot.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import posmy.interview.boot.entity.BooksEntity;
import posmy.interview.boot.entity.BooksQtyEntity;
import posmy.interview.boot.entity.BorrowEntity;
import posmy.interview.boot.entity.MemberEntity;
import posmy.interview.boot.model.BooksModel;
import posmy.interview.boot.repository.BookQtyRepository;
import posmy.interview.boot.repository.BooksRepository;
import posmy.interview.boot.repository.BorrowRepository;
import posmy.interview.boot.repository.MemberRepository;
import posmy.interview.boot.response.ViewBooksResponse;
import posmy.interview.boot.service.LibrarianService;
import posmy.interview.boot.util.StatusHelper;

@Slf4j
@Service
public class LibrarianServiceImpl implements LibrarianService{

	@Autowired
	BooksRepository repo;
	
	@Autowired
	BookQtyRepository repo2;

	@Autowired
	MemberRepository repo3;
	
	@Autowired
	BorrowRepository repo4;
	
	@Override
	public ResponseEntity<?> getAllBooksService() throws Exception {
		log.info(":: getAllBooksService - Started ::");
		ResponseEntity<?> response = null;
		List<BooksEntity> result = new ArrayList<BooksEntity>();
		List<BooksModel> lb = new ArrayList<BooksModel>();
		ViewBooksResponse vbr = new ViewBooksResponse();
		try {
			result = repo.findAllByIsDeleted("N");
			if(result.size() > 0) {
				for(BooksEntity t : result) {
					BooksModel book = new BooksModel();
					
					book.setISBN(t.getIsbn());
					book.setBookName(t.getBookName());
					book.setAuthor(t.getAuthor());
					book.setBooksQty(t.getBooksQty());
					book.setBookStatus(Integer.parseInt(t.getBooksQty())>0?"AVAILABLE": "NOT AVAILABLE");
					lb.add(book);
				}
				vbr.Status = StatusHelper.SUCCESS();
				vbr.Response = lb;
				response = ResponseEntity.status(HttpStatus.OK).body(vbr);
			}
			else {
				response = ResponseEntity.ok(StatusHelper.NO_CONTENT());
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			response = ResponseEntity.ok(StatusHelper.INTERNAL_SERVER_ERROR());
		}
		
		return response;
	}
	
	@Override
	public ResponseEntity<?> getAddBooksService(BooksModel book, String flag) {
		log.info(":: getAddBooksService - Started ::");
		ResponseEntity<?> response = null;
		List<BooksEntity> result = new ArrayList<BooksEntity>();
		BooksEntity bookEnt = new BooksEntity();
		try {
			result = repo.searchBook(book.getISBN(), "N");
			if("add".equals(flag)) {
				if(result.size() > 0) {
					if("Y".equals(result.get(0).getIsDeleted())) {
						bookEnt.setIsbn(book.getISBN());
						bookEnt.setBookName(book.getBookName());
						bookEnt.setAuthor(book.getAuthor());
						bookEnt.setBooksQty(book.getBooksQty());
						bookEnt.setIsDeleted("N");
						repo.save(bookEnt);
						
						for(int i=0; i<Integer.parseInt(book.getBooksQty()); i++) {
							BooksQtyEntity bookQtyEnt = new BooksQtyEntity();
							bookQtyEnt.setIsbn(book.getISBN());
							bookQtyEnt.setStatus("AVAILABLE");
							repo2.save(bookQtyEnt);
						}
						response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM(book.getBookName()+" succesfully added"));
					}
					else {
						// book exist
						int qty = Integer.parseInt(result.get(0).getBooksQty())+Integer.parseInt(book.getBooksQty());
						bookEnt.setIsbn(result.get(0).getIsbn());
						bookEnt.setBookName(book.getBookName());
						bookEnt.setAuthor(book.getAuthor());
						bookEnt.setBooksQty(String.valueOf(qty));
						bookEnt.setIsDeleted("N");
						repo.save(bookEnt);
						
						for(int i=0; i<Integer.parseInt(book.getBooksQty()); i++) {
							BooksQtyEntity bookQtyEnt = new BooksQtyEntity();
							bookQtyEnt.setIsbn(result.get(0).getIsbn());
							bookQtyEnt.setStatus("AVAILABLE");
							repo2.save(bookQtyEnt);
						}
						response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM(book.getBookName()+" succesfully updated"));
					}
					
				}
				else {
					// new book
					bookEnt.setIsbn(book.getISBN());
					bookEnt.setBookName(book.getBookName());
					bookEnt.setAuthor(book.getAuthor());
					bookEnt.setBooksQty(book.getBooksQty());
					bookEnt.setIsDeleted("N");
					repo.save(bookEnt);
					
					for(int i=0; i<Integer.parseInt(book.getBooksQty()); i++) {
						BooksQtyEntity bookQtyEnt = new BooksQtyEntity();
						bookQtyEnt.setIsbn(book.getISBN());
						bookQtyEnt.setStatus("AVAILABLE");
						repo2.save(bookQtyEnt);
					}
					response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM(book.getBookName()+" succesfully added"));
				}
			}
			else if("update".equals(flag)){
				if(result.size() > 0) {
					// book exist
					bookEnt.setIsbn(result.get(0).getIsbn());
					bookEnt.setBookName(book.getBookName());
					bookEnt.setAuthor(book.getAuthor());
					bookEnt.setBooksQty(result.get(0).getBooksQty());
					bookEnt.setIsDeleted(result.get(0).getIsDeleted());
					repo.save(bookEnt);
					response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM(book.getBookName()+" succesfully updated"));
				}
				else {
					// wrong isbn number
					response = ResponseEntity.ok(StatusHelper.NOT_FOUND("ISBN "+book.getISBN()+" not found!"));
				}
			}
			else { // remove book
				if(result.size() > 0) {
					// book exist
					if(!"all".equalsIgnoreCase(book.getBooksQty())) {
						if(Integer.parseInt(book.getBooksQty())>0) {
							if(Integer.parseInt(result.get(0).getBooksQty())>=Integer.parseInt(book.getBooksQty())) {
								int qty = Integer.parseInt(result.get(0).getBooksQty())-Integer.parseInt(book.getBooksQty());
								bookEnt.setIsbn(result.get(0).getIsbn());
								bookEnt.setBookName(result.get(0).getBookName());
								bookEnt.setAuthor(result.get(0).getAuthor());
								bookEnt.setBooksQty(String.valueOf(qty));
								bookEnt.setIsDeleted(result.get(0).getIsDeleted());
								repo.save(bookEnt);
							}
							else {
								return response = ResponseEntity.ok(StatusHelper.BAD_REQUEST("Quantity should be equal to "+result.get(0).getBooksQty()+" or less!"));
							}
						}
						else {// quantity must not be 0
							return response = ResponseEntity.ok(StatusHelper.BAD_REQUEST("Quantity must not be 0 or less!"));
						}
						
						List<BooksQtyEntity> result2 = new ArrayList<BooksQtyEntity>();
						result2 = repo2.findAllByIsbn(book.getISBN());
						if(result2.size()>0) {
							for(int i=0; i<Integer.parseInt(book.getBooksQty()); i++) {
								repo2.deleteById(result2.get(i).getId());
							}
							response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM("ISBN "+book.getISBN()+" succesfully removed"));
						}
					}
					else {// remove all 
						bookEnt.setIsbn(result.get(0).getIsbn());
						bookEnt.setBookName(result.get(0).getBookName());
						bookEnt.setAuthor(result.get(0).getAuthor());
						bookEnt.setBooksQty("0");
						bookEnt.setIsDeleted("Y");
						repo.save(bookEnt);
						
						List<BooksQtyEntity> result2 = new ArrayList<BooksQtyEntity>();
						result2 = repo2.findAllByIsbn(book.getISBN());
						if(result2.size()>0) {
							for(int i=0; i<result2.size(); i++) {
								repo2.deleteById(result2.get(i).getId());
							}
						}
						response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM("ISBN "+book.getISBN()+" succesfully removed"));
					}
				}
				else {
					// wrong isbn number
					response = ResponseEntity.ok(StatusHelper.NOT_FOUND("ISBN "+book.getISBN()+" not found!"));
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			response = ResponseEntity.ok(StatusHelper.INTERNAL_SERVER_ERROR());
		}
		
		return response;
	}

	@Override
	public ResponseEntity<?> getBorrowService(String tel, String iSBN) throws Exception {
		log.info(":: getBorrowService - Started ::");
		ResponseEntity<?> response = null;
		List<BooksEntity> result = new ArrayList<BooksEntity>();
		List<BooksQtyEntity> result2 = new ArrayList<BooksQtyEntity>();
		List<MemberEntity> result3 = new ArrayList<MemberEntity>();
		BooksEntity be = new BooksEntity();
		BooksQtyEntity bqe = new BooksQtyEntity();
		BorrowEntity boe = new BorrowEntity();
		
		try {
			result = repo.searchBook(iSBN, "N");
			if(result.size()>0) {
				result2 = repo2.findAllByIsbnAndStatus(iSBN, "AVAILABLE");
				result3 = repo3.searchTelAndAccountstatus(tel, "ACTIVE");
				if(result2.size()>0 && result3.size()>0) {
					// update book qty table
					bqe.setId(result2.get(0).getId());
					bqe.setIsbn(result2.get(0).getIsbn());
					bqe.setStatus("BORROWED");
					repo2.save(bqe);
					
					// update book table
					int qty = Integer.parseInt(result.get(0).getBooksQty())-1;
					be.setIsbn(result.get(0).getIsbn());
					be.setBookName(result.get(0).getBookName());
					be.setAuthor(result.get(0).getAuthor());
					be.setBooksQty(String.valueOf(qty));
					be.setIsDeleted(result.get(0).getIsDeleted());
					repo.save(be);
					
					//insert new borrower
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DATE, 7);
					Date date = cal.getTime(); 
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String t = sdf.format(date);
					
					boe.setIsbn(result.get(0).getIsbn());
					boe.setMid(result3.get(0).getMid());
					boe.setDateReturn("N/A");
					boe.setDueDate(t);
					repo4.save(boe);
					
					response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM("Successfully borrowed "+result.get(0).getBookName()
							+". Due date: "+ t));
				}
				else {
					// all book not available or member account not existed
					response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM("ISBN "+iSBN+" is not available or member not existed!"));
				}
			}
			else {
				// wrong isbn number
				response = ResponseEntity.ok(StatusHelper.NOT_FOUND("ISBN "+iSBN+" not found!"));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			response = ResponseEntity.ok(StatusHelper.INTERNAL_SERVER_ERROR());
		}
		
		return response;
	}

	@Override
	public ResponseEntity<?> getReturnService(String tel, String iSBN) throws Exception {
		log.info(":: getReturnService - Started ::");
		ResponseEntity<?> response = null;
		List<BooksEntity> result = new ArrayList<BooksEntity>();
		List<BooksQtyEntity> result2 = new ArrayList<BooksQtyEntity>();
		List<MemberEntity> result3 = new ArrayList<MemberEntity>();
		List<BorrowEntity> result4 = new ArrayList<BorrowEntity>();
		BooksEntity be = new BooksEntity();
		BooksQtyEntity bqe = new BooksQtyEntity();
		BorrowEntity boe = new BorrowEntity();
		
		try {
			result3 = repo3.searchTelAndAccountstatus(tel, "ACTIVE");
			if(result3.size()>0) {
				result4 = repo4.findByMidAndDateReturnAndIsbn(result3.get(0).getMid(), "N/A", iSBN);
				if(result4.size()>0) {
					result2 = repo2.findAllByIsbnAndStatus(iSBN, "BORROWED");
					result = repo.searchBook(iSBN, "N");
					if(result2.size()>0 && result.size()>0) {
						//insert new borrower
						Calendar cal = Calendar.getInstance();
						Date date = cal.getTime(); 
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						String t = sdf.format(date);
						boe.setBid(result4.get(0).getBid());
						boe.setIsbn(result4.get(0).getIsbn());
						boe.setMid(result4.get(0).getMid());
						boe.setDateReturn(t);
						boe.setDueDate(result4.get(0).getDueDate());
						repo4.save(boe);
						
						// update book qty table
						bqe.setId(result2.get(0).getId());
						bqe.setIsbn(result2.get(0).getIsbn());
						bqe.setStatus("AVAILABLE");
						repo2.save(bqe); 
						
						// update book table
						int qty = Integer.parseInt(result.get(0).getBooksQty())+1;
						be.setIsbn(result.get(0).getIsbn());
						be.setBookName(result.get(0).getBookName());
						be.setAuthor(result.get(0).getAuthor());
						be.setBooksQty(String.valueOf(qty));
						be.setIsDeleted(result.get(0).getIsDeleted());
						repo.save(be);
						
						response = ResponseEntity.ok(StatusHelper.SUCCESS_CUSTOM("Successfully returned "+result.get(0).getBookName()
								+". Returned date: "+ t));
					}
					else {
						// book not found
						response = ResponseEntity.ok(StatusHelper.NOT_FOUND("ISBN "+iSBN+" not found!"));
					}
				}
				else {
					// borrower not found
					response = ResponseEntity.ok(StatusHelper.NOT_FOUND("Record not found!"));
				}
				
			}
			else {
				// member not found
				response = ResponseEntity.ok(StatusHelper.NOT_FOUND("Record not found!"));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			response = ResponseEntity.ok(StatusHelper.INTERNAL_SERVER_ERROR());
		}
		return response;
	}

}
