package posmy.interview.boot.service;

import org.springframework.http.ResponseEntity;

import posmy.interview.boot.model.BooksModel;

public interface LibrarianService {

	public ResponseEntity<?> getAddBooksService(BooksModel book, String flag) throws Exception;

	public ResponseEntity<?> getAllBooksService() throws Exception;

	public ResponseEntity<?> getBorrowService(String tel, String iSBN) throws Exception;

	public ResponseEntity<?> getReturnService(String tel, String iSBN) throws Exception;

}
