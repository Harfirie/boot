package posmy.interview.boot.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonPropertyOrder({"Name", "Tel", "Email", "AccountStatus"})
public class MemberModel {

	@JsonProperty("Name")
	String name;
	
	@JsonProperty("Tel")
	String tel;
	
	@JsonProperty("Email")
	String email;
	
	@JsonProperty("AccountStatus")
	String accountStatus;
}
