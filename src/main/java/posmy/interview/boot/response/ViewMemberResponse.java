package posmy.interview.boot.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;

import posmy.interview.boot.model.MemberModel;

@JsonPropertyOrder({"Status", "Response"})
public class ViewMemberResponse {

	@JsonSetter("Status")
	public BaseStatus Status;
	
	@JsonSetter("Response")
	public List<MemberModel> Response;
}
