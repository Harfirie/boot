package posmy.interview.boot.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;

import posmy.interview.boot.model.BooksModel;

@JsonPropertyOrder({"Status", "Response"})
public class ViewBooksResponse {

	@JsonSetter("Status")
	public BaseStatus Status;
	
	@JsonSetter("Response")
	public List<BooksModel> Response;

}
