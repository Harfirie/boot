package posmy.interview.boot.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Code", "Type", "Timestamp", "Error", "Message"})
public class BaseStatus {

	@JsonProperty("Code")
	Integer code;
	
	@JsonProperty("Type")
	String type;
	
	@JsonProperty("Error")
	String error;
	
	@JsonProperty("Timestamp")
	Long timestamp;
	
	@JsonProperty("Message")
	String message;
}
