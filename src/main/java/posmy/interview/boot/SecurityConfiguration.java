package posmy.interview.boot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		auth.inMemoryAuthentication()
		.withUser("admin")
		.password("$2a$10$tqeXxwzYL8ipOaA5fokeUO0EaE0hfyNHH.5/lba9jA82/sPgLCMCi")
		.roles("LIBRARIAN")
		.and()
		.withUser("user")
		.password("$2a$10$SH8J1ydTLdlS7ePC.WjEnulA3UuOOc0hhVdqBvNmaYvvxJcX7mz/u")
		.roles("MEMBER");
	}
	 
	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http.csrf().disable().authorizeRequests()
		.antMatchers("/").permitAll()
		.antMatchers("/book/**").hasRole("LIBRARIAN")
		.antMatchers("/member/**").hasRole("MEMBER")
		.and().formLogin();
		
//		http.httpBasic();
//		http.addFilterAfter(new CsrfLoggerFilter(), CsrfLoggerFilter.class);
	}
}
